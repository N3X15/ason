import unittest

from tests.smoke import SmokeTests
from tests.sample import READMESamples
from tests.int import IntegerTests

if __name__ == "__main__":
    unittest.run()
