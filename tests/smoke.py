import math
import unittest
from io import StringIO
from ason.asontypes.obj import AsonObject

from ason.encoder import AsonEncoder


class SmokeTests(unittest.TestCase):
    def test_smoke_dict(self) -> None:
        orig = {
            "int": 2023,
            "bool": True,
            "float": math.pi,
            "str": "Jesus Christ, what have I done?",
        }
        o = ""
        with StringIO() as f:
            d = AsonEncoder()
            d.write(orig, f)
            o = f.getvalue()
        data = AsonEncoder().decode(o)
        self.assertDictEqual(orig, data)
    # def test_smoke_obj(self) -> None:
    #     class AsonTestObject(AsonObject):
    #         ASON_TYPE_ID = '6fb4bcbc-2f21-425d-a522-5159fbae58e5'
    #         def __init__(self) -> None:
    #             super().__init__()
    #             self.a: int = 2023
    #             self.b: bool = True
    #             self.c: float = math.pi
    #             self.d: str = 'Jesus Christ, what have I done?'

    #     obj = AsonTestObject()
    #     o = ""
    #     with StringIO() as f:
    #         d = AsonEncoder()
    #         d.write(obj, f)
    #         o = f.getvalue()
    #     dec = AsonEncoder()
    #     dec.registerObjectType(AsonTestObject)
    #     data: AsonTestObject = dec.decode(o)
    #     self.assertDictEqual(obj.a, data.a)
    #     self.assertDictEqual(obj.b, data.b)
    #     self.assertDictEqual(obj.c, data.c)
    #     self.assertDictEqual(obj.d, data.d)
