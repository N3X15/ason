import collections
import unittest
from typing import OrderedDict

import ason


class READMESamples(unittest.TestCase):
    def test_readme_1(self):
        o: str = ason.dumps({"a": 1, "b": 2.5, "c": True, "d": "A longer string."})
        expected = "d4S1ai1S1bFGK4S1ctS1dSG1A longer string."
        self.assertEqual(expected, o)

    def test_readme_2(self):
        o: OrderedDict = ason.loads("d4S1ai1S1bFGK4S1ctS1dSG1A longer string.")

        expected = collections.OrderedDict(
            [("a", 1), ("b", 2.5), ("c", True), ("d", "A longer string.")]
        )
        self.assertEqual(expected, o)

    def test_readme_3(self):
        # ints are encoded as 4-bit integers with a 5th bit,
        # used to tell the parser whether to continue reading characters.
        assert ason.dumps(1) == "i1"
        assert ason.dumps(-1) == "i-1"
        assert ason.dumps(123) == "iR7"
        # String constants are prefixed with 'S' and the 4-bit encoded length of the string in characters (NOT bytes).
        assert ason.dumps("") == "S0"
        assert ason.dumps("a") == "S1a"
        # Floats are prefixed with 'F', converted to bytes (double-format for accuracy), read as a big-endian integer, encoded as an ASON integer.
        assert ason.dumps(1.5) == "FVJOF"
        # True booleans are presented as the letter 't'
        assert ason.dumps(True) == "t"
        # False booleans are presented as the letter 'f'
        assert ason.dumps(False) == "f"
        # Dictionaries are prefixed with 'd' and the 4-bit encoded length of the dictionary.
        # Each key-value pair then follows.
        assert ason.dumps(dict()) == "d0"
        assert ason.dumps(dict(a=2)) == "d1S1ai2"
        # Lists are prefixed with 'l' and the 4-bit encoded length of the list
        assert ason.dumps([]) == "l0"
        assert ason.dumps(["a"]) == "l1S1a"
