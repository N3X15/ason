from io import StringIO
from typing import Callable, TextIO
import unittest

from ason.encoder import AsonEncoder


class IntegerTests(unittest.TestCase):
    def _stream_to_string(self, func: Callable[[int, TextIO], None], data: int) -> str:
        with StringIO() as f:
            func(data, f)
            return f.getvalue()

    def test_int_to_string_0(self) -> None:
        inp: int = 0
        expected: str = "0"
        enc = AsonEncoder()
        actual: str = enc.int.encode(inp)
        self.assertEqual(actual, expected)

    def test_int_to_string_1(self) -> None:
        inp: int = 1
        expected: str = "1"
        enc = AsonEncoder()
        actual: str = enc.int.encode(inp)
        self.assertEqual(actual, expected)

    def test_int_to_string_neg1(self) -> None:
        inp: int = -1
        expected: str = "-1"
        enc = AsonEncoder()
        actual: str = enc.int.encode(inp)
        self.assertEqual(actual, expected)

    def test_int_to_string_255(self) -> None:
        inp: int = 0xff
        expected: str = "VF"
        enc = AsonEncoder()
        actual: str = enc.int.encode(inp)
        self.assertEqual(actual, expected)

    def test_string_to_int_255(self) -> None:
        inp: str = "VF"
        expected: int = 0xff
        enc = AsonEncoder()
        actual: str = enc.int.decode(inp)
        self.assertEqual(actual, expected)
