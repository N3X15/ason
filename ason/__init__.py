from io import StringIO
from typing import Any, TextIO
from ason.asontypes.obj import AsonObjectDataDictionary, AsonObjectEncoder

from ason.encoder import AsonEncoder
from ason.typecodes import ETypeCode


def dumps(input_: Any) -> str:
    with StringIO() as f:
        dump(input_, f)
        return f.getvalue()
        
def loads(data: str) -> Any:
    with StringIO(data) as f:
        return load(f)
    
def load(f: TextIO) -> Any:
    e = AsonEncoder()
    o = e.read(f)
    if not isinstance(o, AsonObjectEncoder):
        return o
    return e.read(f)


def dump(data: Any, f: TextIO) -> None:
    e = AsonEncoder()
    e.collectTypesFrom(data)
    e.writeDataDictionary(f, not_if_empty=True)
    e.write(data, f)
