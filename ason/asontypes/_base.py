from __future__ import annotations

from abc import ABC, abstractmethod
from typing import TYPE_CHECKING, Any, TextIO

if TYPE_CHECKING:
    from ason.encoder import AsonEncoder


class BaseAsonType(ABC):
    def __init__(self, enc: AsonEncoder) -> None:
        self.encoder: AsonEncoder = enc

    @abstractmethod
    def encode(self, value: Any) -> str:
        pass

    @abstractmethod
    def decode(self, data: str) -> Any:
        pass

    def encodeStream(self, f: TextIO, value: Any) -> None:
        f.write(self.encode(value))

    @abstractmethod
    def decodeStream(self, f: TextIO) -> Any:
        pass
