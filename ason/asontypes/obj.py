from __future__ import annotations

import inspect
from io import StringIO
from typing import TYPE_CHECKING, Any, Dict, List, Optional, Set, TextIO, Type

from ason.asontypes._base import BaseAsonType
from ason.typecodes import ETypeCode

if TYPE_CHECKING:
    from ason.encoder import AsonEncoder


class AsonObjectEncoder(BaseAsonType):
    DDID_VERSION: int = 0

    def __init__(self, enc: AsonEncoder) -> None:
        super().__init__(enc)
        self.TYPE2ID: Dict[AsonObjectDataDictionary, str] = {}
        self.ID2TYPE: Dict[str, AsonObjectDataDictionary] = {}
        self.ALL_TYPES: List[AsonObjectDataDictionary] = []

    def registerDataDictionary(self, add: AsonObjectDataDictionary) -> None:
        if add.id in self.ID2TYPE.keys():
            add.ddid = self.ID2TYPE[add.id]
        else:
            add.ddid = len(self.ALL_TYPES)
            self.ALL_TYPES.append(add)
            self.ID2TYPE[add.id] = add
            self.TYPE2ID[add] = add.id

    def getDataDictionaryFor(
        self, data: AsonObject
    ) -> Optional[AsonObjectDataDictionary]:
        return self.ID2TYPE.get(data.getAsonTypeID())

    def getOrCreateDataDictionaryFor(
        self, data: AsonObject
    ) -> Optional[AsonObjectDataDictionary]:
        atid = data.getAsonTypeID()
        if atid not in self.ID2TYPE.keys():
            self.registerDataDictionary(AsonObjectDataDictionary.BuildFromObject(data))
        return self.ID2TYPE.get(data.getAsonTypeID())

    def getDataDictionaryByDDID(self, ddid: int) -> Optional[AsonObjectDataDictionary]:
        return self.ALL_TYPES[ddid]

    def encode(self, data: AsonObject) -> str:
        add = self.getDataDictionaryFor(data)
        o: str = ""
        if add is None:
            d = {}
            for f in data.getAsonSerializableFields():
                d[f.name] = getattr(data, f.name)
            o += self.encoder.encodeNone()  # No ADD
            o += self.encoder.encodeString(data.getAsonTypeID())
            o += self.encoder.encodeDict(d)
        else:
            o += self.encoder.encodeInteger(add.ddid)
            o += add.encodeBody(data)
        return o

    def decode(self, data: list) -> List[Any]:
        with StringIO(data) as f:
            return self.decodeStream(f)

    def encodeStream(self, f: TextIO, value: AsonObject) -> None:
        add = self.getDataDictionaryFor(value)
        o: str = ""
        if add is None:
            d = {}
            for f in value.getAsonSerializableFields():
                d[f.name] = getattr(data, f.name)
            o += self.encoder.encodeNone()  # No ADD
            o += self.encoder.encodeString(data.getAsonTypeID())
            o += self.encoder.encodeDict(d)
        else:
            o += self.encoder.encodeInteger(add.ddid)
            o += add.encodeBodyStream(value)
        return o

    def decodeStream(self, f: TextIO) -> List[Any]:
        ddid: Optional[int]
        if (ddid := self.encoder.read()) is None:
            o = AsonAnonymousObject()
            o.typeID = self.encoder.readString()
            for k, v in self.encoder.readDict():
                assert k not in ("typeID",)
                setattr(o, k, v)
            return o
        else:
            add = self.ALL_TYPES[ddid]
            o = add.type()
            data = {}
            allowedFields: Set[str] = set(
                [fld.name for fld in o.getAsonSerializableFields()]
            )
            for fld in add.fields:
                if fld.name not in allowedFields:
                    continue
                data[fld.name] = f.read()
            o.deserializeAsonData(data)
            return o

    def writeDataDictionary(self, f: TextIO) -> None:
        f.write(self.encoder.int.encode(len(self.ALL_TYPES)))
        for i, dde in enumerate(self.ALL_TYPES):
            dde.writeToDataDictionary(self.encoder, i, f)

    def readDataDictionary(self, f: TextIO) -> None:
        dde = None
        for i in range(self.encoder.readInteger(f, skip_id=True)):
            dde = AsonObjectDataDictionary()
            dde.readDataDictionary(self.encoder, i, f)
        return self


class AsonObjectFieldAssociation:
    def __init__(self) -> None:
        self.ddfid: int = 0
        self.name: str = ""
        self.type: ETypeCode = ETypeCode.NONE

    # def encodeToDataDictionary(self, encoder: AsonEncoder) -> str:
    #     o = encoder.encodeString(self.name)
    #     o += encoder.encodeTypeID(self.type)
    #     return o
    def writeToDataDictionary(self, encoder: AsonEncoder, f: TextIO) -> None:
        f.write(encoder.str.encode(self.name))
        encoder.writeTypeID(self.type, f)

    def readFromDataDictionary(self, encoder: AsonEncoder, f: TextIO, i: int) -> None:
        self.ddfid = i
        self.name = encoder.readString(f, skip_id=True)
        self.type = encoder.readTypeID(f)


class AsonObjectDataDictionary:
    def __init__(self) -> None:
        self.id: str = ""
        self.ddid: int = 0
        self.type: Optional[Type[AsonObject]] = None
        self.fields: List[AsonObjectFieldAssociation] = []

    def encodeToDataDictionary(self, encoder: AsonEncoder) -> str:
        o = encoder.str.encode(self.id)
        o += encoder.int.encode(len(self.fields))
        for fld in self.fields:
            o += fld.encodeToDataDictionary(encoder)
        return o

    def writeToDataDictionary(self, encoder: AsonEncoder, f: TextIO) -> None:
        f.write(encoder.str.encode(self.id))
        f.write(encoder.int.encode(len(self.fields)))
        for fld in self.fields:
            fld.writeToDataDictionary(encoder, f)

    def readDataDictionary(self, encoder: AsonEncoder, i: int, f: TextIO) -> None:
        self.id = encoder.readString(f, skip_id=True)
        self.ddid = i
        for ddfid in range(encoder.readInteger(f, skip_id=True)):
            fld = AsonObjectFieldAssociation()
            fld.readFromDataDictionary(encoder, fld, ddfid)
            self.fields.append(fld)

    def encodeBodyStream(
        self, encoder: AsonEncoder, f: TextIO, value: AsonObject
    ) -> None:
        encoder.writeInteger(self.ddid, f)
        encoder.registerObjectType(value.__class__)
        dd = AsonObjectDataDictionary.BuildFromObject(value)
        value.serializeAsonData(dd, f)

    @classmethod
    def BuildFromObject(self, obj: AsonObject) -> AsonObjectDataDictionary:
        ddt = AsonObjectDataDictionary()
        ddt.id = obj.getAsonTypeID()
        ddt.type = type(obj)
        ddt.fields = obj.getAsonSerializableFields()
        return ddt


class AsonObject:
    ASON_TYPE_ID: Optional[str] = None
    ASON_SERIALIZABLE_FIELDS: Optional[List[str]] = None

    def __init__(self) -> None:
        pass

    @classmethod
    def getAsonTypeID(self) -> str:
        if self.ASON_TYPE_ID is not None:
            return self.ASON_TYPE_ID
        cls = self.__class__
        mod = cls.__module__
        if mod == "builtins":
            return cls.__qualname__
        return f"{mod}.{cls.__qualname__}"

    def getAsonSerializableFields(self) -> List[AsonObjectFieldAssociation]:
        o = []
        for fld in inspect.getmembers(self):
            if fld[0].startswith("_"):  # private or protected
                continue
            fa = AsonObjectFieldAssociation()
            fa.name = fld[0]
            fa.type = ETypeCode.NONE
            o.append(fa)
        return o

    def deserializeAsonData(self, data: Dict[str, Any]) -> None:
        for k, v in data.items():
            setattr(self, k, v)

    def serializeAsonData(self, dd: AsonObjectDataDictionary) -> Dict[str, Any]:
        o: Dict[str, Any] = {}
        for field in dd.fields:
            o[field.name] = getattr(self, field.name, None)
        return o


class AsonAnonymousObject:
    def __init__(self) -> None:
        self.typeID: str = ""
